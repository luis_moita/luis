#código para visualização de planilhas sem utilizar a frramenta excel
#para utiliza-lo é necessário adicionar a tabela .xlsx na mesma pasta em que o código será executado
import pandas as pd
df = pd.read_excel('base_de_dados.xlsx')     #inserir o nome do arquivo excel o lugar de "base_de_dados"
pd.set_option('display.max_columns', 500)    #define o maximo de colunas a serem exibidas no preview
pd.set_option("max_rows", None)              #define o maximo de linhas a serem exibidas no preview
n = 1000                                     #variavel para numero de linhas exibidas   
df.head(n)                                   #exibe as 'n' primeiras linhas do código 